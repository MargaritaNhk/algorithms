package com.algorithms.homework.homework01;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Node<T> {
    T value;
    Node<T> next;

    public Node(T value) {
        this(value, null);
    }

    protected boolean canEqual(Object other) {
        return other instanceof Node;
    }

}
