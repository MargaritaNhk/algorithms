package com.algorithms.homework.homework01.task2;

import com.algorithms.homework.homework01.Stack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class BracketsBalanceChecker {

    static boolean check(String str) {
        HashMap<Character, Character> brackets = new HashMap<>();
        brackets.put('{', '}');
        brackets.put('[', ']');
        brackets.put('(', ')');
        Stack<Character> stack = new Stack<>();
        for (Character elem : str.toCharArray()) {
            if (brackets.containsKey(elem)) {
                stack.push(elem);
            } else if (brackets.containsValue(elem)) {  // No need in this condition if all strings contain brackets ONLY
                if (brackets.get(stack.pop()) != elem) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(check(reader.readLine()));
    }
}
