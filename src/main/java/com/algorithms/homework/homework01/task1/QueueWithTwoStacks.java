package com.algorithms.homework.homework01.task1;

import com.algorithms.homework.homework01.Stack;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueueWithTwoStacks<T> {

    private Stack<T> pushStack;
    private Stack<T> popStack;

    void queue(T value) {
        pushStack.push(value);
    }

    T dequeue() {
        if (popStack.isEmpty()) {
            while (!pushStack.isEmpty()) {
                popStack.push(pushStack.pop());
            }
        }
        return popStack.pop();
    }

    boolean isEmpty() {
        return popStack.isEmpty() && pushStack.isEmpty();
    }

    int size() {
        return pushStack.size() + popStack.size();
    }
}
