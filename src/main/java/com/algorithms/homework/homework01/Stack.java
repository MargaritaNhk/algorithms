package com.algorithms.homework.homework01;

public class Stack<T> {
    private Node<T> head;
    private int size;

    public void push(T value) {
        head = new Node<>(value, head);
        size++;
    }

    public T pop() {
        if (head == null) return null;
        T value = head.value;
        head = head.next;
        size--;
        return value;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        return size;
    }
}
