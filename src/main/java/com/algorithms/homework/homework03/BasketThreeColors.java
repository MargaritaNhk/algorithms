package com.algorithms.homework.homework03;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum BasketThreeColors {
    RED, WHITE, GREEN;

    public static final List<BasketThreeColors> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    public static final int SIZE = VALUES.size();
}
