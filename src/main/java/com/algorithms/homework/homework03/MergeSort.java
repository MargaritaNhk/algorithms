package com.algorithms.homework.homework03;

import java.util.Arrays;

@SuppressWarnings({"unchecked", "WeakerAccess"})
public class MergeSort {

    public static void sort(Comparable[] array, Comparable[] aux, int start, int end) {
        if (end > start) {
            int middle = start + end >>> 1;
            sort(array, aux, start, middle);
            sort(array, aux, middle + 1, end);
            merge(array, aux, start, end, middle);
        }
    }

    public static void sort(Comparable[] array) {
        Comparable[] aux = new Comparable[array.length];
        sort(array, aux, 0, array.length - 1);
    }

    public static void merge(Comparable[] array, Comparable[] aux, int start, int end, int middle) {
        System.arraycopy(array, 0, aux, 0, array.length);
        int i = start;
        int j = middle + 1;
        for (int k = start; k <= end; k++) {
            if (i <= middle && j <= end) {
                array[k] = aux[i].compareTo(aux[j]) > 0 ? aux[j++] : aux[i++];
            } else if (i > middle) {
                array[k] = aux[j++];
            } else {
                array[k] = aux[i++];
            }
        }
    }

    public static void main(String[] args) {
        String[] testOne = new String[]{"I", "C", "H", "B", "F", "E", "G", "G", "J", "A", "K", "D"};
        MergeSort.sort(testOne);
        System.out.println(Arrays.toString(testOne));
    }

}
