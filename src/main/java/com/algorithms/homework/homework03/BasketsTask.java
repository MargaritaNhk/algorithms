package com.algorithms.homework.homework03;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;


@NoArgsConstructor
@AllArgsConstructor
public class BasketsTask {

    private static final Random RANDOM = new Random();
    private Basket[] baskets;


    @Data
    @AllArgsConstructor
    static class Basket implements Comparable<Basket> {

        private BasketThreeColors color;

        @Override
        public int compareTo(Basket basket) {
            return color.compareTo(basket.getColor());
        }

        @Override
        public String toString() {
            return "" + color;
        }
    }

    void generate(int size) {
        int v;
        baskets = new Basket[size];
        for (int i = 0; i < size; i++) {
            v = RANDOM.nextInt(BasketThreeColors.SIZE);
            baskets[i] = new Basket(BasketThreeColors.VALUES.get(v));
        }
    }

    void generateWithNullValues(int size) {
        int v;
        baskets = new Basket[size];
        for (int i = 0; i < size; i++) {
            v = RANDOM.nextInt(BasketThreeColors.SIZE + 1);
            baskets[i] = v != BasketThreeColors.SIZE ? new Basket(BasketThreeColors.VALUES.get(v)) : new Basket(null);
        }
    }

    void sort(int start, int end) {
        int firstWhite = end;
        for (int i = start; i <= end; i++) {
            if (baskets[i].getColor().equals(BasketThreeColors.WHITE)) {
                firstWhite = i;
                break;
            }
        }

        if (firstWhite == end) {
            return;
        }

        swap(0, firstWhite);
        Basket b = baskets[0];
        int i = 1, tb = end, lb = 0;
        while (tb >= i) {
            if (baskets[i].compareTo(b) < 0) {
                swap(i++, lb++);
            } else if (baskets[i].compareTo(b) > 0) {
                swap(i, tb--);
            } else {
                i++;
            }
        }
    }

    void sort() {
        this.sort(0, baskets.length - 1);
    }

    void moveNullBasketsToEndAndSort() {
        int tb = baskets.length - 1, i = 0;
        while (!BasketThreeColors.VALUES.contains(baskets[tb].getColor()) && tb > 0) {
            tb--;
        }
        while (i < tb) {
            if (!BasketThreeColors.VALUES.contains(baskets[i].getColor())) {
                swap(i, tb);
                while (!BasketThreeColors.VALUES.contains(baskets[tb].getColor()) && tb > 0) {
                    tb--;
                }
            }
            i++;
        }
        sort(0, tb);
    }

    void swap(int i, int j) {
        Basket temp = baskets[i];
        baskets[i] = baskets[j];
        baskets[j] = temp;
    }

    void printBaskets() {
        System.out.println(Arrays.stream(baskets).map(Basket::toString).collect(Collectors.joining(" ", "[", "]")));
    }

    public static void main(String[] args) {

        BasketsTask threeColoredBasketTask = new BasketsTask();
        threeColoredBasketTask.generate(30);
        System.out.println("Three colored baskets:");
        System.out.print(Arrays.toString(threeColoredBasketTask.baskets));
        threeColoredBasketTask.sort();
        System.out.println("\nSorted baskets:");
        System.out.print(Arrays.toString(threeColoredBasketTask.baskets));
        System.out.println("\n");

        BasketsTask threeColoredBasketTasWithNull = new BasketsTask();
        threeColoredBasketTasWithNull.generateWithNullValues(50);
        System.out.println("Three colored and null baskets:");
        threeColoredBasketTasWithNull.printBaskets();
        threeColoredBasketTasWithNull.moveNullBasketsToEndAndSort();
        System.out.println("Sorted baskets:");
        threeColoredBasketTasWithNull.printBaskets();
    }
}
