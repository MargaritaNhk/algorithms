package com.algorithms.homework.homework02;

import java.util.*;

@SuppressWarnings({"unchecked", "WeakerAccess"})
public class Sort {

    public static <E extends Comparable<E>> void bubbleSort(Collection<E> collection) {
        E[] array = (E[]) collection.toArray(new Comparable[collection.size()]);
        bubbleSort(array);
        collection.clear();
        Collections.addAll(collection, array);

    }

    public static <E extends Comparable<E>> void bubbleSort(E[] array) {
        boolean needNextSortingStep = true;
        for (int i = 1; i < array.length && needNextSortingStep; i++) {
            needNextSortingStep = false;
            for (int j = 0; j < array.length - i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    E temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                    needNextSortingStep = true;
                }
            }
        }
    }

    public static <E extends Comparable<E>> void selectionSort(Collection<E> collection) {
        E[] array = (E[]) collection.toArray(new Comparable[collection.size()]);
        selectionSort(array);
        collection.clear();
        Collections.addAll(collection, array);

    }

    public static <E extends Comparable<E>> void selectionSort(E[] list) {
        for (int i = 0; i < list.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < list.length; j++) {
                if (list[j].compareTo(list[min]) < 0) {
                    min = j;
                }
            }
            E temp = list[min];
            list[min] = list[i];
            list[i] = temp;
        }
    }

    public static <E extends Comparable<E>> void insertionSort(Collection<E> collection) {
        E[] array = (E[]) collection.toArray(new Comparable[collection.size()]);
        insertionSort(array);
        collection.clear();
        Collections.addAll(collection, array);
    }

    public static <E extends Comparable<E>> void insertionSort(E[] array) {
        int index;
        E elem;
        for (int i = 1; i < array.length; i++) {
            elem = array[i];
            index = i;
            while (index > 0 && array[index - 1].compareTo(elem) > 0) {
                array[index] = array[index - 1];
                index--;
            }
            array[index] = elem;
        }
    }


    public static void main(String[] args) {
        List<String> testOne = new ArrayList<>(Arrays.asList("D", "C", "A", "B", "F", "E"));
        Sort.bubbleSort(testOne);
        System.out.println(testOne);

        String[] testTwo = new String[]{"D", "C", "A", "B", "F", "E"};
        Sort.bubbleSort(testTwo);
        System.out.println(Arrays.asList(testTwo));

        List<String> testThree = new ArrayList<>(Arrays.asList("D", "C", "A", "B", "F", "E"));
        Sort.selectionSort(testThree);
        System.out.println(testThree);

        String[] testFour = new String[]{"D", "C", "A", "B", "F", "E"};
        Sort.selectionSort(testFour);
        System.out.println(Arrays.asList(testFour));

        String[] testFive = new String[]{"D", "C", "A", "B", "F", "E"};
        Sort.insertionSort(testFive);
        System.out.println(Arrays.asList(testFive));

        List<String> testSix = new ArrayList<>(Arrays.asList("D", "C", "A", "B", "F", "E"));
        Sort.selectionSort(testSix);
        System.out.println(testSix);

    }
}

