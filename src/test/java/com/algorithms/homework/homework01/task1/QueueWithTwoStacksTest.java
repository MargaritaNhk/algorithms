package com.algorithms.homework.homework01.task1;

import com.algorithms.homework.homework01.Stack;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class QueueWithTwoStacksTest {

    private final Stack<String> pushStringStack;
    private final Stack<String> popStringStack;
    private final QueueWithTwoStacks<String> queueFinal = new QueueWithTwoStacks<>();

    {
        Stack<String> stack = new Stack<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        pushStringStack = stack;
        popStringStack = stack;
        queueFinal.setPushStack(pushStringStack);
        queueFinal.setPopStack(popStringStack);
    }


    @Test
    public void QueueOfStringsWithTwoStacksTest() {

        QueueWithTwoStacks<String> queue = new QueueWithTwoStacks<>();
        queue.setPopStack(new Stack<>());
        queue.setPushStack(new Stack<>());
        queue.queue("5");
        queue.queue("4");
        queue.queue("3");
        queue.queue("2");
        queue.queue("1");
        queue.dequeue();
        queue.queue("1");
        queue.queue("2");
        queue.dequeue();
        queue.queue("3");
        assertEquals(queueFinal, queue);
        queue.queue("5");
        queue.queue("2");
        queue.queue("1");
        queue.dequeue();
        queue.queue("1");
        queue.queue("2");
        queue.dequeue();
        queue.queue("3");
        queue.queue("5");
        queue.queue("2");
        queue.queue("3");
        while (!queue.isEmpty()) {
            queue.dequeue();
        }
        assertEquals(new QueueWithTwoStacks<String>(new Stack<>(), new Stack<>()), queue);
    }
}
